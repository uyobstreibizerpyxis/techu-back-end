var config = require('../config/config');
var express = require('express');
var router = express.Router();

var user_services = require(config.user_services_impl);

router.get('/users', (req, res) => {
  let callBack = function(result) {
    res.json(result);
  }
  user_services.getAll(req.query,callBack);
});

router.post('/users', (req, res) => {
  let user = req.body;
  user.password = req.headers.password;
  let callBack = function(user) {
    res.send({ 'message': 'OK', 'user': user });
  }
  let result = user_services.add(user,callBack); 
});

router.get('/users/:id', (req, res) => {
  let callBack = function(user) {
    if (!user) {
      res.status(404).send({ 'message': 'No existe el usuario' });
    } else {
      res.send(user);
    }
  }
  user_services.getById(req.params.id,callBack);
});

router.put('/users/:id', (req, res) => {
  let user = req.body;
  user.password = req.headers.password;
  let callBack = function(user) {
    if (!user) {
      res.status(404).send({ 'message': 'No existe el usuario' });
    } else {
      res.send({ 'message': 'ok', user });
    }
  }
  user_services.update(req.params.id, user, callBack);
});

router.delete('/users/:id', (req, res) => {
  let callBack = function(user) {
    if (!user) {
      res.status(404).send({ 'message': 'No existe el usuario' });
    } else {
      res.send({ 'message': 'Usuario eliminado' });
    }
  }
  user_services.delte(req.params.id,callBack)
});


module.exports.router = router;
