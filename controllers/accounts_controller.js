var config = require('../config/config');
var express = require('express');
var router = express.Router();

var account_services = require(config.account_services_impl);

router.get('/accounts', (req, res) => {
  req.query.user_id = req.user.user.id;
  let callBack = function(result) {
    res.json(result);
  }
  account_services.getAll(req.query,callBack);
});

router.post('/accounts', (req, res) => {
  let account = req.body;
  account.user_id = req.user.user.id;
  let callBack = function(account) {
    res.send({ 'message': 'OK', 'account': account });
  }
  let result = account_services.add(account,callBack); 
});

router.get('/accounts/:id', (req, res) => {
  let callBack = function(account) {
    if (!account) {
      res.status(404).send({ 'message': 'No existe la cuenta' });
    } else {
      res.send(account);
    }
  }
  account_services.getById(req.params.id,callBack);
});

router.put('/accounts/:id', (req, res) => {
  let account = req.body;
  account.password = req.headers.password;
  let callBack = function(account) {
    if (!account) {
      res.status(404).send({ 'message': 'No existe la cuenta' });
    } else {
      res.send({ 'message': 'ok', account });
    }
  }
  account_services.update(req.params.id, account, callBack);
});

router.delete('/accounts/:id', (req, res) => {
  let callBack = function(account) {
    if (!account) {
      res.status(404).send({ 'message': 'No existe la cuenta' });
    } else {
      res.send({ 'message': 'Usuario eliminado' });
    }
  }
  account_services.delte(req.params.id,callBack)
});


module.exports.router = router;
