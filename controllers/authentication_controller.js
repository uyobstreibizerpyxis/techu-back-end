var config = require('../config/config');
var express = require('express');
var router = express.Router();
var user_services = require(config.user_services_impl);
var token_services = require(config.token_services_impl);
var account_services = require(config.account_services_impl);



router.post('/authentication/login', (req, res) => {
  let email = req.body.email
    , password = req.body.password;
  let callBack = function(user) {
    if (!user) {
      res.status(404).send({ 'message': 'No existe el usuario' });
    } else {
      let callBackAdd = function(token) {
        res.status(200).send({
          'user': user,
          'token': token
        });
      };
      token_services.add({ user: user }, callBackAdd);
    }
  }
  user_services.getOne({ 'email': email, 'password': password }, callBack);
});

router.post('/authentication/singin', (req, res) => {
  let email = req.body.email
    , password = req.body.password,
    avatar = "https://robohash.org/" + email + "?size=50x50",
    first_name = req.body.first_name,
    last_name = req.body.last_name;
  let callBack = function(user) {
    if (!user) {
      res.status(404).send({ 'message': 'No existe el usuario' });
    } else {
      let _accoutns = [];
      _accoutns.push({
        user_id: user.id,
        type: "Caja de Ahorros",
        currency: "UYU",
        amount: 0
      });
      _accoutns.push({
        user_id: user.id,
        type: "Caja de Ahorros",
        currency: "USD",
        amount: 0
      });
      _accoutns.push({
        user_id: user.id,
        type: "Cuenta Corriente",
        currency: "UYU",
        amount: 0
      });
      _accoutns.push({
        user_id: user.id,
        type: "Cuenta Corriente",
        currency: "USD",
        amount: 0
      });

      account_services.add(_accoutns);
      res.status(200).send(user);
    }
  }
  user_services.add({ 'email': email, 'password': password, 'avatar': avatar, 'first_name': first_name, 'last_name': last_name }, callBack);
});

router.post('/authentication/logout', (req, res) => {
  let callBack = function(token) {
    res.status(200).send({
      'token': token
    });
  }
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
    token = req.headers.authorization.split(' ')[1];
    token_services.remove(token, callBack);
  }
});

module.exports.router = router;
