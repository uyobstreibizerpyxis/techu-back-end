var config = require('../config/config');
var express = require('express');
var router = express.Router();

var transaction_services = require(config.transaction_services_impl);

router.get('/transactions', (req, res) => {
  req.query.user_id = req.user.user.id;
  let callBack = function(result) {
    res.json(result);
  }
  transaction_services.getAll(req.query,callBack);
});

router.get('/transactions/:idAccount', (req, res) => {
  let account={
    user_id: req.user.user.id,
    id: req.params.idAccount
  };
  let startDate = req.query.startDate;
  let endDate = req.query.endDate;
  let callBack = function(result) {
    res.json(result);
  }
  transaction_services.getByAccountId(account,startDate,endDate,callBack);
});

router.post('/transactions', (req, res) => {
  let transaction = req.body;
  transaction.user_id = req.user.user.id;
  let callBack = function(transaction) {
    res.send({ 'message': 'OK', 'transaction': transaction });
  }
  let result = transaction_services.add(transaction,callBack); 
});

router.get('/transactions/detail/:id', (req, res) => {
  let callBack = function(transaction) {
    if (!transaction) {
      res.status(404).send({ 'message': 'No existe la cuenta' });
    } else {
      res.send(transaction);
    }
  }
  transaction_services.getById(req.params.id,callBack);
});

module.exports.router = router;
