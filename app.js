var express = require('express'); 

var cors = require('cors'); 
var bodyParser = require('body-parser');
var app = express();
var config = require('./config/config');
var jwt_config = require('./config/jwt_config');

app.use(cors({
  "origin": "*",
  "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
  "preflightContinue": false,
  "optionsSuccessStatus": 204
}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

let user_controller = require('./controllers/user_controller');
let authentication_controller = require('./controllers/authentication_controller');
let accounts_controller = require('./controllers/accounts_controller');
let transactions_controller = require('./controllers/transactions_controller');


app.use(jwt_config.express_jwt);
app.use(config.base_usri, user_controller.router);
app.use(config.base_usri, authentication_controller.router);
app.use(config.base_usri, accounts_controller.router);
app.use(config.base_usri, transactions_controller.router);


var port = process.env.PORT || 3000;
app.listen(port);
console.log('Magic happens on port ' + port);