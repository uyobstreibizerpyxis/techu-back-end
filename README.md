## Back-end

__Configuracion__

Para configurar el servicio iremos al archivo config/config.js, ahi encontraremos, **url base**, **MLAB api key** y el selector de que implementacion de servicios utilizar.

__Capa de servicios__

Se realizo una capa de servicios la cual se encuentra contenida dentro de services. Esta capa cuenta con 2 implementaciones, mlab la cual invoca a la api data de mlab y mocks que meneja todo en memoria, esta ultima sirve para que los desarrolladores pueden trabajar sin la necesidad de tener conexion a internet

__Utils__

Se cearon varios utiles auxiliareas para encapsular logica reutilizable y facilitar la integracion con servicios externos. 

- currency converter client:
  - Classe que concentra la logica para poder invocar al servicio free.currencyconverterapi.com, que nos facilita la conversion entre monedas
- mlab cliente:
  - Classe que consentra toda la logica para incovar todos los metodos de la api data de mlab

__Seguridad__

El servicio cuenta con seguridad por token JWT, para obtener el token se debe hacer un login con email y contraseña en el endpoint /authentication/login. Para hacer el logout se necesita hacer un request al endpoint /authentication/logout. todos los metodos estan protegidos menos **/authentication/login** y **/authentication/singin** que son publicos.

__Endpoints__

### /accounts
>**get**
>> retorna todas las cuentas del usuario dueño del token, por query params recive filtros de igualacion para hacer querys como por **ejemplo** id=1 devuelta la cuenta con ese id, o type=Caja de Ahorros para obtener todas las cuenta corrientes 

>post
>>crea una cuenta para el usuario dueño del token

### /accounts/:id
>get
>>retorna toda la informacion de la cuenta con el id que se le para por url

>put
>>actualiza toda la informacion de la cuenta con el id que se le para por url

>delete
>>elimina la cuenta con el id que se le para por url

### /transactions
>**get**
>> retorna todas los movimientos del usuario dueño del token, por query params recive filtros de igualacion para hacer querys como por **ejemplo** id=1 devuelta la cuenta con ese id, o sentAmount=12 para obtener todas las transacciones que enviaron 12

>post
>>crea una transaccion para el usuario dueño del token

### /transactions/:idAcount
>get
>>retorna todos los movimientos asociados a la cuenta que recive por parametro en la url

### /transactions/detail/:id
>get
>>retorna toda la informacion de la transaccion con el id que se le para por url

### /authentication/login
> post
>> metodo para el login del usuario
### /authentication/singin
> post
>> metodo para registrar un nuevo usuario
### /authentication/logout
> post
>> metodo para hacer logout de un usuario ya logueado


__Commandas__

*  npm run start
    + Corre la aplicacion con el archivo mock

* npm run start-mlab
    + Corre la aplicacion apuntano a mlab


__Doker__

El servicio backend cuenta con un docker file el cual expone el puerto 8080

__Ejemplo__

#### Login
> localhost:3000/api-uruguay/v1/authentication/login
>> { "email" : "hpointin0@i2i.jp", "password" : "test" }