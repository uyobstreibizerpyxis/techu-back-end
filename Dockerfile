#Imagen docker base
FROM node:latest

#Crear directorio de trabajo
WORKDIR /docker-api

#Copiar directorio de trabajo al directorio del workdir anterior
ADD . /docker-api

#Instala dependencias del proyecto
# RUN npm install

#Expone la imagen en puerto 3000
EXPOSE 8080

CMD ["npm", "run", "start-mlab"]