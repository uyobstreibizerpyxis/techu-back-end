var mlab_client_module = require('../../utils/mlab_client')
var mlab_params_class = mlab_client_module.mlab_params_class;

class user_services {
  getAll(query, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.query = query;
    let mlab_client = mlab_client_module.mlab_client_class;
    mlab_client.get('user', mlab_params, (err, resM, body) => {
      let result;
      if (!err) {
        result = body;
      }
      callBack(result);
    });
  }

  getOne(query, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.query = query;
    mlab_params.findOne = true;
    mlab_params.fields = {
      "id": 1,
      "first_name": 1,
      "last_name": 1,
      "email": 1,
      "avatar": 1
    };
    let mlab_client = mlab_client_module.mlab_client_class;
    mlab_client.get('user', mlab_params, (err, resM, body) => {
      let result;
      if (!err) {
        result = body;
      }
      callBack(result);
    });
  }

  getById(id, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.query = { 'id': parseInt(id) };
    mlab_params.findOne = true;
    let mlab_client = mlab_client_module.mlab_client_class;
    mlab_client.get('user', mlab_params, (err, resM, body) => {
      let user;
      if (!err) {
        user = body;
      }
      callBack(user);
    });
  }

  add(user, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.sort = { 'id': -1 };
    mlab_params.limit = 1;
    let mlab_client = mlab_client_module.mlab_client_class;

    mlab_client.get('user', mlab_params, (errorGet, messageGet, bodyGet) => {
      if (!errorGet) {
        let newID = bodyGet[0].id + 1;
        user.id = newID;
        mlab_client.post('user', user, (errorPost, messageGetPost, bodyPost) => {
          let result;
          if (!errorPost) {
            result = bodyPost;
          }
          callBack(result);
        });
      } else {
        callBack(null);
      }
    });
  }

  update(id, user, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.query = { 'id': parseInt(id) };
    mlab_params.findOne = true;
    let mlab_client = mlab_client_module.mlab_client_class;
    mlab_client.get('user', mlab_params, (errorGet, messageGet, bodyGet) => {
      if (!errorGet) {
        user.id = bodyGet.id;
        mlab_client.put('user', bodyGet._id.$oid, user, (errorPost, messageGetPost, bodyPost) => {
          let result;
          if (!errorPost) {
            result = bodyPost;
          }
          callBack(result);
        });
      } else {
        callBack(null);
      }
    });
  }

  delte(id, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.query = { 'id': parseInt(id) };
    let mlab_client = mlab_client_module.mlab_client_class;
    mlab_client.get('user', mlab_params, (errorGet, messageGet, bodyGet) => {
      if (!errorGet) {
        mlab_client.delete('user', bodyGet[0]._id.$oid, (errorPost, messageGetPost, bodyPost) => {
          let result;
          if (!errorPost) {
            result = bodyPost[0];
          }
          callBack(result);
        });
      } else {
        callBack(null);
      }
    });
  }
}

module.exports = new user_services();