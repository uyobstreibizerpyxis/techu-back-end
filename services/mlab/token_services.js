var jwt = require('jsonwebtoken');
var config = require('../../config/config');
var user_services = require('./user_services');

var tokens = [];

class token_services {
  add(payload, callBack) {
    let token = jwt.sign(payload, config.secret_token);
    // jwt.verify(token, config.secret_token, function(err, decoded) {
    //   let callBackGetOne = function(user) {
    //     if (user) {
    //       if (!user.tokens) {
    //         user.tokens = [];
    //       }
    //       user.tokens.push({'id': decoded.iat, 'token':token});
    //       let callBackUpdate = function(user) {
    //         if (user) {
              callBack(token);
    //         } else {
    //           callBack(null);
    //         }
    //       }
    //       user_services.update(user.id, user, callBackUpdate);
    //     } else {
    //       callBack(null);
    //     }
    //   }
    //   user_services.getOne({'id':decoded.user.id}, callBackGetOne);
    // });
  }

  remove(token, callBack) {
    // jwt.verify(token, config.secret_token, function(err, decoded) {
    //   let callBackGetOne = function(user) {
    //     if (user && user.tokens) {
    //       let index = user.tokens.indexOf(user.tokens.find(t=> t.id = decoded.iat));
    //       if (index > -1) {
    //         user.tokens.splice(index, 1);
    //       }
    //       let callBackUpdate = function(user) {
    //         if (user) {
              callBack(token);
    //         } else {
    //           callBack(null);
    //         }
    //       }
    //       user_services.update(user.id, user, callBackUpdate);
    //     } else {
    //       callBack(null);
    //     }
    //   }
    //   user_services.getOne({'id':decoded.user.id}, callBackGetOne);
    // });
  }

  valid(token, callBack) {
    jwt.verify(token, config.secret_token, function(err, decoded) {
      // let callBackGetOne = function(user) {
      //   if (user && user.tokens) {
      //     callBack(!!user.tokens.find(t=> t.id = decoded.iat));
      //   } else {
      //     callBack(false);
      //   }
      // }
      // user_services.getOne({'id':decoded.user.id}, callBackGetOne);
      callBack(true);
    });
  }
}

module.exports = new token_services();