var mlab_client_module = require('../../utils/mlab_client')
var mlab_params_class = mlab_client_module.mlab_params_class;
var accounts_services = require('./accounts_services');
var currency_converter_params_class = require('../../utils/currency_converter_client').currency_converter_params_class;
var currency_converter_client_class = require('../../utils/currency_converter_client').currency_converter_client_class;


class transactions_services {
  getAll(query, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.query = query;
    let mlab_client = mlab_client_module.mlab_client_class;
    mlab_client.get('transactions', mlab_params, (err, resM, body) => {
      let result;
      if (!err) {
        result = body;
      }
      callBack(result);
    });
  }

  getByAccountId(account, startDate, endDate, callBack) {
    let mlab_params = new mlab_params_class();
    let query = {
      "$and": [
        { "$or": [{ "originAccount.id": parseInt(account.id) }, { "destinyAccount.id": parseInt(account.id) }] },
        { "$or": [{ "originAccount.user_id": parseInt(account.user_id) }, { "destinyAccount.user_id": parseInt(account.user_id) }] }
      ]
    };

    let createDateQuery = {}
    if (startDate) {
      createDateQuery["$gte"] = new Date(startDate)
      query["createDate"] = createDateQuery;
    }
    if (endDate) {
      createDateQuery["$lt"] = new Date(endDate)
      query["createDate"] = createDateQuery;
    }
    mlab_params.query = query;
    mlab_params.sort = { createDate: -1 };
    let mlab_client = mlab_client_module.mlab_client_class;
    mlab_client.get('transactions', mlab_params, (err, resM, body) => {
      let result;
      if (!err) {
        result = body;
      }
      callBack(result);
    });
  }

  getOne(query, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.query = query;
    mlab_params.findOne = true;
    let mlab_client = mlab_client_module.mlab_client_class;
    mlab_client.get('transactions', mlab_params, (err, resM, body) => {
      let result;
      if (!err) {
        result = body;
      }
      callBack(result);
    });
  }

  getById(id, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.query = { 'id': parseInt(id) };
    mlab_params.findOne = true;
    let mlab_client = mlab_client_module.mlab_client_class;
    mlab_client.get('transactions', mlab_params, (err, resM, body) => {
      let transaction;
      if (!err) {
        transaction = body;
      }
      callBack(transaction);
    });
  }

  add(transaction, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.sort = { 'id': -1 };
    mlab_params.limit = 1;
    transaction.createDate = new Date();
    let mlab_client = mlab_client_module.mlab_client_class;

    let save = function() {
      mlab_client.get('transactions', mlab_params, (errorGet, messageGet, bodyGet) => {
        if (!errorGet) {
          let newID = bodyGet.length > 0 ? bodyGet[0].id + 1 : 1;
          transaction.id = newID;
          mlab_client.post('transactions', transaction, (errorPost, messageGetPost, bodyPost) => {
            let result;
            if (!errorPost) {
              result = bodyPost;
            }
            callBack(result);
          });
        } else {
          callBack(null);
        }
      });
    }

    let obteinsAccounts = function(accoutns) {
      let currency_converter_params = new currency_converter_params_class();
      for (var accoutn in accoutns) {
        if (accoutns[accoutn].id == transaction.originAccount.id) {
          transaction.originAccount = accoutns[accoutn];
          transaction.originAccount.amount = Number(transaction.originAccount.amount) - Number(transaction.sentAmount);
          currency_converter_params.from = transaction.originAccount.currency;
          accounts_services.update(transaction.originAccount.id, transaction.originAccount, function(){
            console.log("updateAccount");
          });
        } else if (accoutns[accoutn].id == transaction.destinyAccount.id) {
          transaction.destinyAccount = accoutns[accoutn];
          currency_converter_params.to = transaction.destinyAccount.currency;
        }
      }

      if (transaction.originAccount.currency != transaction.destinyAccount.currency) {
        let exchangeRate = function(err, resM, body) {
          let rate = 0;
          for (var property in body.results) {
            rate = Number(body.results[property].val);
          }
          transaction.reciveAmount = Number(transaction.sentAmount) * rate;
          transaction.destinyAccount.amount += transaction.reciveAmount;
          accounts_services.update(transaction.destinyAccount.id, transaction.destinyAccount, function(){
            console.log("updateAccount");
          });
          save();
        }
        currency_converter_client_class.convert(currency_converter_params, exchangeRate);
      } else {
        transaction.reciveAmount = Number(transaction.sentAmount);
        transaction.destinyAccount.amount = Number(transaction.destinyAccount.amount) + transaction.reciveAmount;
        accounts_services.update(transaction.destinyAccount.id, transaction.destinyAccount, function(){
          console.log("updateAccount");
        });
        save();
      }
    };

    accounts_services.getAll({ "$or": [{ "id": parseInt(transaction.originAccount.id) }, { "id": parseInt(transaction.destinyAccount.id) }] }, obteinsAccounts)
  }

  update(id, transaction, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.query = { 'id': parseInt(id) };
    mlab_params.findOne = true;
    let mlab_client = mlab_client_module.mlab_client_class;
    mlab_client.get('transactions', mlab_params, (errorGet, messageGet, bodyGet) => {
      if (!errorGet) {
        transaction.id = bodyGet.id;
        mlab_client.put('transactions', bodyGet._id.$oid, transaction, (errorPost, messageGetPost, bodyPost) => {
          let result;
          if (!errorPost) {
            result = bodyPost;
          }
          callBack(result);
        });
      } else {
        callBack(null);
      }
    });
  }

  delte(id, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.query = { 'id': parseInt(id) };
    let mlab_client = mlab_client_module.mlab_client_class;
    mlab_client.get('transactions', mlab_params, (errorGet, messageGet, bodyGet) => {
      if (!errorGet) {
        mlab_client.delete('transactions', bodyGet[0]._id.$oid, (errorPost, messageGetPost, bodyPost) => {
          let result;
          if (!errorPost) {
            result = bodyPost[0];
          }
          callBack(result);
        });
      } else {
        callBack(null);
      }
    });
  }
}

module.exports = new transactions_services();