var mlab_client_module = require('../../utils/mlab_client')
var mlab_params_class = mlab_client_module.mlab_params_class;

class accounts_services {
  getAll(query, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.query = query;
    let mlab_client = mlab_client_module.mlab_client_class;
    mlab_client.get('accounts', mlab_params, (err, resM, body) => {
      let result;
      if (!err) {
        result = body;
      }
      callBack(result);
    });
  }

  getOne(query,callBack){
    let mlab_params = new mlab_params_class();
    mlab_params.query = query;
    mlab_params.findOne = true;
    let mlab_client = mlab_client_module.mlab_client_class;
    mlab_client.get('accounts', mlab_params, (err, resM, body) => {
      let result;
      if (!err) {
        result = body;
      }
      callBack(result);
    });
  }

  getById(id, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.query = { 'id': parseInt(id) };
    mlab_params.findOne = true;
    let mlab_client = mlab_client_module.mlab_client_class;
    mlab_client.get('accounts', mlab_params, (err, resM, body) => {
      let account;
      if (!err) {
        account = body;
      }
      callBack(account);
    });
  }

  add(account, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.sort = { 'id': -1 };
    mlab_params.limit = 1;
    let mlab_client = mlab_client_module.mlab_client_class;

    mlab_client.get('accounts', mlab_params, (errorGet, messageGet, bodyGet) => {
      if (!errorGet) {
        if(Array.isArray(account)){
          let newID = bodyGet.length>0? bodyGet[0].id:0;
          account.forEach(function(element, index, array){
            newID ++;
            element.id = newID;
          });
          mlab_client.post('accounts', account, (errorPost, messageGetPost, bodyPost) => {
            let result;
            if (!errorPost) {
              result = bodyPost;
            }
            if(callBack)
              callBack(result);
          });
        } else {
          let newID = bodyGet.length>0? bodyGet[0].id + 1:1;
          account.id = newID;
          mlab_client.post('accounts', account, (errorPost, messageGetPost, bodyPost) => {
            let result;
            if (!errorPost) {
              result = bodyPost;
            }
            if(callBack)
            callBack(result);
          });
        }
      } else {
        callBack(null);
      }
    });
  }

  update(id, account, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.query = { 'id': parseInt(id) };
    mlab_params.findOne = true;
    let mlab_client = mlab_client_module.mlab_client_class;
    mlab_client.get('accounts', mlab_params, (errorGet, messageGet, bodyGet) => {
      if (!errorGet && bodyGet) {
        account.id = bodyGet.id;
        mlab_client.put('accounts', bodyGet._id.$oid, account, (errorPost, messageGetPost, bodyPost) => {
          let result;
          if (!errorPost) {
            result = bodyPost;
          }
          callBack(result);
        });
      } else {
        callBack(null);
      }
    });
  }

  delte(id, callBack) {
    let mlab_params = new mlab_params_class();
    mlab_params.query = { 'id': parseInt(id) };
    let mlab_client = mlab_client_module.mlab_client_class;
    mlab_client.get('accounts', mlab_params, (errorGet, messageGet, bodyGet) => {
      if (!errorGet) {
        mlab_client.delete('accounts', bodyGet[0]._id.$oid, (errorPost, messageGetPost, bodyPost) => {
          let result;
          if (!errorPost) {
            result = bodyPost[0];
          }
          callBack(result);
        });
      } else {
        callBack(null);
      }
    });
  }
}

module.exports = new accounts_services();