var mockFile = require('../../mocks/MOCK_TRANSACTIONS.json');

class transactions_services {
  getAll(query, callBack) {
    let filterByQuery = function(transaction, query) {
      for (var key in query) {
        if (typeof transaction[key] === typeof query[key] && typeof query[key] != 'object' && transaction[key] != query[key])
          return false;
        else if (typeof transaction[key] === typeof query[key] && typeof query[key] == 'object')
          return filterByQuery(transaction[key], query[key]);
      }
      return true;
    }
    let result = mockFile.filter(transaction => filterByQuery(transaction, query));
    callBack(result);
  }


  getByAccountId(account, callBack) {
    let filterByQuery = function(transaction, query) {
      for (var key in query) {
        if (typeof transaction[key] === typeof query[key] && typeof query[key] != 'object' && transaction[key] != query[key])
          return false;
        else if (typeof transaction[key] === typeof query[key] && typeof query[key] == 'object')
          return filterByQuery(transaction[key], query[key]);
      }
      return true;
    }
    let result = mockFile.filter(transaction => filterByQuery(transaction, { originAccount: account }) || filterByQuery(transaction, { destinyAccount: account }));
    result.sort(function(a, b) {
      a = new Date(a.createDate);
      b = new Date(b.createDate);
      return a > b ? -1 : a < b ? 1 : 0;
    });
    callBack(result);
  }

  getOne(query, callBack) {
    let filterByQuery = function(transaction) {
      for (var key in query) {
        if (transaction[key] != query[key])
          return false;
      }
      return true;
    }
    let result = mockFile.filter(transaction => filterByQuery(transaction));
    callBack(result[0]);
  }

  getById(id, callBack) {
    let transaction = mockFile.find(x => x.id == id);
    callBack(transaction);
  }

  add(transaction, callBack) {
    transaction.id = mockFile.length > 0 ? mockFile[mockFile.length - 1].id + 1 : 1;
    mockFile.push(transaction);
    callBack(transaction);
  }

  update(id, transaction, callBack) {
    let result = mockFile.find(x => x.id == id);
    if (result) {
      for (var property in transaction) {
        result[property] = transaction[property];
      }
    }
    callBack(result);
  }

  delte(id, callBack) {
    let transaction = mockFile.find(x => x.id == id);
    if (transaction) {
      let index = mockFile.indexOf(transaction);
      if (index > -1) {
        mockFile.splice(index, 1);
      }
    }
    callBack(transaction);
  }
}

module.exports = new transactions_services();