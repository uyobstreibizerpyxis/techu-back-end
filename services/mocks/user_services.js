var mockFile = require('../../mocks/MOCK_USER.json');

class user_services{
  getAll(query,callBack){
    let filterByQuery = function(usr) {
      for (var key in query) {
        if (usr[key] != query[key])
          return false;
      }
      return true;
    }
    let result = mockFile.filter(usr => filterByQuery(usr));
    callBack(result);
  }

  getOne(query,callBack){
    let filterByQuery = function(usr) {
      for (var key in query) {
        if (usr[key] != query[key])
          return false;
      }
      return true;
    }
    let result = mockFile.filter(usr => filterByQuery(usr));
    callBack(result[0]);
  }

  getById(id, callBack){
    let user = mockFile.find(x => x.id == id);
    callBack(user);
  }

  add(user,callBack){
    user.id = mockFile[mockFile.length-1].id+1;
    mockFile.push(user);
    callBack(user);
  }

  update(id, user,callBack){
    let result = mockFile.find(x => x.id == id);
    if (result) {
      for (var property in user) {
        result[property] = user[property];
      }
    }
    callBack(result);
  }

  delte(id, callBack){
    let user = mockFile.find(x => x.id == id);
    if(user){
      let index = mockFile.indexOf(user);
      if (index > -1) {
        mockFile.splice(index, 1);
      }
    }
    callBack(user);
  }
}

module.exports = new user_services();