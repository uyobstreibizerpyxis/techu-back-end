var jwt = require('jsonwebtoken');
var config = require('../../config/config');


var tokens = [];

class token_services {
  add(payload, callBack) {
    let token = jwt.sign(payload, config.secret_token);
    jwt.verify(token, config.secret_token, function(err, decoded) {
      tokens[decoded.iat] = token;
    });
    callBack(token);
  }

  remove(token, callBack) {
    jwt.verify(token, config.secret_token, function(err, decoded) {
      tokens.splice(decoded.iat, 1);
    });
    callBack(token);
  }

  valid(token, callBack) {
    jwt.verify(token, config.secret_token, function(err, decoded) {
      callBack(!!tokens[decoded.iat]);
    });
  }
}

module.exports = new token_services();