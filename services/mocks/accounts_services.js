var mockFile = require('../../mocks/MOCK_ACCOUNTS.json');

class accounts_services{
  getAll(query,callBack){
    let filterByQuery = function(account) {
      for (var key in query) {
        if (account[key] != query[key])
          return false;
      }
      return true;
    }
    let result = mockFile.filter(account => filterByQuery(account));
    callBack(result);
  }

  getOne(query,callBack){
    let filterByQuery = function(account) {
      for (var key in query) {
        if (account[key] != query[key])
          return false;
      }
      return true;
    }
    let result = mockFile.filter(account => filterByQuery(account));
    callBack(result[0]);
  }

  getById(id, callBack){
    let account = mockFile.find(x => x.id == id);
    callBack(account);
  }

  add(account,callBack){
    account.id = mockFile.length>0? mockFile[mockFile.length-1].id+1: 1;
    mockFile.push(account);
    callBack(account);
  }

  update(id, account,callBack){
    let result = mockFile.find(x => x.id == id);
    if (result) {
      for (var property in account) {
        result[property] = account[property];
      }
    }
    callBack(result);
  }

  delte(id, callBack){
    let account = mockFile.find(x => x.id == id);
    if(account){
      let index = mockFile.indexOf(account);
      if (index > -1) {
        mockFile.splice(index, 1);
      }
    }
    callBack(account);
  }
}

module.exports = new accounts_services();