var fs = require('fs');

let _users = [];
let _accoutns = [];
let _transactions = [];

function readUsers() {
  if (!this._users) {
    _users = require('./MOCK_USER.json');
  }
  return _users;
}

function readAcctouns() {
  if (!this._accoutns) {
    _accoutns = require('./MOCK_ACCOUNTS.json');
  }
  return _accoutns;
}

function generateAccoutns() {
  readUsers()
  _users.forEach(element => {
    _accoutns.push({
      id: _accoutns.length + 1,
      user_id: element.id,
      type: "Caja de Ahorros",
      currency: "UYU",
      amount: 200
    });
    _accoutns.push({
      id: _accoutns.length + 1,
      user_id: element.id,
      type: "Caja de Ahorros",
      currency: "USD",
      amount: 200
    });
    _accoutns.push({
      id: _accoutns.length + 1,
      user_id: element.id,
      type: "Cuenta Corriente",
      currency: "UYU",
      amount: 200
    });
    _accoutns.push({
      id: _accoutns.length + 1,
      user_id: element.id,
      type: "Cuenta Corriente",
      currency: "USD",
      amount: 200
    });
  });
  saveJsonToFile(_accoutns, "./mocks/MOCK_ACCOUNTS.json");
}

function genereateTransactions() {
  readAcctouns();
  _accoutns.forEach(element => {
    var cantTransactions = getRandomInt(10, 20);
    for (var i = 0; i <= cantTransactions; i++) {
      var transfer = {};
      transfer.id= _transactions.length + 1,
      transfer.destinyType = getRandomType();
      transfer.destinyType.amount = undefined;
      transfer.sentAmount = getRandomInt(20, 200);
      transfer.originAccount = element;
      transfer.originAccount.amount = undefined;
      transfer.createDate = getRandomDate(new Date(2018, 4, 1), new Date())
      if (transfer.destinyType == 'CP') {
        transfer.destinyAccount = getRandomAccount(_accoutns.filter(x=> x.user_id == element.user_id && x.id != element.id));
      } else if (transfer.destinyType == 'CBBVA') {
        transfer.destinyAccount = getRandomAccount(_accoutns.filter(x=> x.user_id != element.user_id));
      } else if (transfer.destinyType == 'CT') {

      }
      _transactions.push(transfer);
    }
  });
  saveJsonToFile(_transactions, "./mocks/MOCK_TRANSACTIONS.json");
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function getRandomType() {
  switch (Math.floor(Math.random() * (1 - 3)) + 3) {
    case 1:
      return 'CP';
    case 2:
      return 'CBBVA';
    case 3:
      return 'CT';
  }
}

function getRandomAccount(accounts) {
  var index = Math.floor(Math.random() * (accounts.length));
  return accounts[index];
}

function getRandomDate(start, end) {
  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

function saveJsonToFile(elments, fileName){
  console.log(fileName);
  console.log(elments.length);
  let json_file = "[";
  elments.forEach(element => {
    json_file += JSON.stringify(element);
    if (elments.indexOf(element) != (elments.length - 1)) {
      json_file += ",\n";
    }
  });
  json_file += "]";
  console.log(json_file);
  fs.writeFile(fileName, json_file);
}

// generateAccoutns();
// genereateTransactions();