var express_jwt = require('express-jwt');
var config = require('./config');
var token_services = require(config.token_services_impl);

var tokens = [];

var isRevokedCallback = function(req, payload, done) {
  let callBack = function(valid) {
    return done(null, !valid);
  }
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
    token = req.headers.authorization.split(' ')[1];
    token_services.valid(token, callBack);
  }
};

module.exports.express_jwt = express_jwt({ secret: config.secret_token, isRevoked: isRevokedCallback })
  .unless({ path: [config.base_usri + '/authentication/login', config.base_usri + '/authentication/singin'] })
