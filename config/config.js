var optimist = require('optimist');
let secret_token = 'techU';
let MLAB_API_KEY = "kJtnR1T9OmylCWXRsv-wfc91gDp3oFwS";
let base_usri = '/api-uruguay/v1'

let mlab_enable = false;
if(optimist.argv.mlab){
  mlab_enable = (optimist.argv.mlab == 'true');
}
let user_services_impl = '../services/'+(mlab_enable? 'mlab': 'mocks')+'/user_services';
let token_services_impl = '../services/'+(mlab_enable? 'mlab': 'mocks')+'/token_services';
let account_services_impl = '../services/'+(mlab_enable? 'mlab': 'mocks')+'/accounts_services';
let transaction_services_impl = '../services/'+(mlab_enable? 'mlab': 'mocks')+'/transactions_services';

module.exports.MLAB_API_KEY = MLAB_API_KEY;
module.exports.user_services_impl = user_services_impl;
module.exports.token_services_impl = token_services_impl;
module.exports.account_services_impl = account_services_impl;
module.exports.transaction_services_impl = transaction_services_impl;
module.exports.secret_token = secret_token;
module.exports.base_usri = base_usri;
