var requestJson = require('request-json');
var config = require('../config/config');
class mlab_params_class {
  /**
   * q=<query> - restrict results by the specified JSON query
   * c=true - return the result count for this query
   * f=<set of fields> - specify the set of fields to include or exclude in each document (1 - include; 0 - exclude)
   * fo=true - return a single document from the result set (same as findOne() using the mongo shell
   * s=<sort order> - specify the order in which to sort each specified field (1- ascending; -1 - descending)
   * sk=<num results to skip> - specify the number of results to skip in the result set; useful for paging
   * l=<limit> - specify the limit for the number of results (default is 1000)
   */
  constructor() {
    this.query;
    this.count;
    this.fields;
    this.findOne;
    this.sort;
    this.skip;
    this.limit;
  }

  getAsQueryParams(){
    let result = '';
    if(this.query){
      result += `&q=${ JSON.stringify(this.query) }`;
    }
    if(this.fields){
      result += `&f=${ JSON.stringify(this.fields) }`;
    }
    if(this.sort){
      result += `&s=${ JSON.stringify(this.sort) }`;
    }
    if(this.count){
      result += `&c=${ this.count }`;
    } 
    if(this.findOne){
      result += `&fo=${ this.findOne }`;
    } 
    if(this.skip){
      result += `&sk=${ this.skip }`;
    } 
    if(this.limit){
      result += `&l=${ this.limit }`;
    } 
    return result;
  }
}

class mlab_client_class {
  constructor(_MLAB_API_KEY) {
    this._MLAB_API_KEY = _MLAB_API_KEY? _MLAB_API_KEY : "kJtnR1T9OmylCWXRsv-wfc91gDp3oFwS";
  }
  
  _mlabUrl(collection) {
    return this._mlabUrl(collection, null);
  }

  _mlabUrl(collection, _id) {
    let DB = "techu";
    let API_KEY_PARAM = "apiKey=" + this._MLAB_API_KEY;
    let URL_BASE_MLAB = `https://api.mlab.com/api/1/databases/${ DB }/collections/${ collection }`;
    if(_id){
      URL_BASE_MLAB += `/${ _id }`;
    }
    return URL_BASE_MLAB + "?" + API_KEY_PARAM;
  }

  get(collection, params, callback) {
    let URL_REQUEST = this._mlabUrl(collection);
    if (params) {
      URL_REQUEST += params.getAsQueryParams();
    }
    let clienteMlab = requestJson.createClient(URL_REQUEST);
    clienteMlab.get('', (err, resM, body) => callback(err, resM, body));
  }

  post(collection, body, callback) {
    let URL_REQUEST = this._mlabUrl(collection);
    let clienteMlab = requestJson.createClient(URL_REQUEST);
    clienteMlab.post('', body, (errPost, resMPost, bodyPost) => callback(errPost, resMPost, bodyPost));
  }

  put(collection, id,body, callback) {
    let URL_REQUEST = this._mlabUrl(collection,id);
    let clienteMlab = requestJson.createClient(URL_REQUEST);
    var test = function(errPut, resMPut, bodyPut){
      callback(errPut, resMPut, bodyPut);
    };
    clienteMlab.put('', body, (errPut, resMPut, bodyPut) => test(errPut, resMPut, bodyPut));
  }

  delete(collection, id, callback) {
    let URL_REQUEST = this._mlabUrl(collection,id);
    let clienteMlab = requestJson.createClient(URL_REQUEST);
    clienteMlab.delete('', (errDelete, resMDelete, bodyDelete) => callback(errDelete, resMDelete, bodyDelete));
  }

}
module.exports.mlab_params_class = mlab_params_class;
module.exports.mlab_client_class = new mlab_client_class(config.MLAB_API_KEY);