var requestJson = require('request-json');
var config = require('../config/config');
class currency_converter_params_class {

  constructor() {
    this.from;
    this.to;
  }

  getAsQueryParams(){
    let result = '';
    if(this.from && this.to){
      result += `q=${ this.from }_${ this.to }`;
    } 
    return result;
  }
}

class currency_converter_client_class {

  _currency_converterUrl(endpoint) {
    let URL_BASE_MLAB = `http://free.currencyconverterapi.com/api/v6/${endpoint}`;
    return URL_BASE_MLAB + "?";
  }

  convert(params, callback) {
    let URL_REQUEST = this._currency_converterUrl('convert');
    if (params) {
      URL_REQUEST += params.getAsQueryParams();
    }
    let clienteMlab = requestJson.createClient(URL_REQUEST);
    console.log(URL_REQUEST);
    clienteMlab.get('', (err, resM, body) => callback(err, resM, body));
  }

}
module.exports.currency_converter_params_class = currency_converter_params_class;
module.exports.currency_converter_client_class = new currency_converter_client_class();